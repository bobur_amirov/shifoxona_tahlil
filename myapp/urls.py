from django.urls import path
from .views import home, alomatlar, kasallik

urlpatterns = [
    path('', home, name='home'),
    path('alomatlar', alomatlar, name='alomatlar'),
    path('kasallik', kasallik, name='kasallik')
]
