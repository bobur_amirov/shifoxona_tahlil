from django.contrib import admin
from .models import Alomat, KasalikTable

# Register your models here.
admin.site.register(Alomat)
admin.site.register(KasalikTable)

# class KasalikTableAdmin(admin.ModelAdmin):
#     model = KasalikTable
#     list_display = ['name','alomatlar']
# admin.site.register(KasalikTable, KasalikTableAdmin)