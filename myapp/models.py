from django.db import models

# Create your models here.

class Alomat(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class KasalikTable(models.Model):
    name = models.CharField(max_length=200)
    alomatlar = models.ManyToManyField(Alomat)

    def __str__(self):
        return self.name
