from django.shortcuts import render
from .models import Alomat, KasalikTable

# Create your views here.
def home(request):
    return render(request, 'home.html')

def alomatlar(request):
    alomatlar = Alomat.objects.all()
    # print(request.GET)   
    return render(request, 'alomatlar.html', {'alomatlar': alomatlar,})

def kasallik(request):
    # print(request.GET)
    alomatlar_count = []
    kasalllik_alomatlar_counts = KasalikTable.objects.all()
    for i in kasalllik_alomatlar_counts:
        alomatlar_count.append(i.alomatlar.count())

    print(alomatlar_count)
    alomat = []
    
    alomat.append(request.GET.keys())  
    print(alomat)
    for i in alomat[0]:
        print(int(i))
    
    kasalliklar = KasalikTable.objects.filter(alomatlar__in=alomat[0])
    kasal = []
    for k in kasalliklar:
        kasal.append(k.name)

    k1 = kasal.count('Kasallik1') / alomatlar_count[0] * 100
    k2 = kasal.count('Kasallik2') / alomatlar_count[1] * 100
    k3 = kasal.count('kasallik3') / alomatlar_count[2] * 100
    k4 = kasal.count('kasallik4') / alomatlar_count[3] * 100
    k5 = kasal.count('kasallik5') / alomatlar_count[4] * 100
    k6 = kasal.count('kasallik6')
    k7 = kasal.count('kasallik7')
    k8 = kasal.count('kasallik8')
    k9 = kasal.count('kasallik9')
    k10 = kasal.count('kasallik10')
    return render(request, 'kasallik.html', {'kasalliklar': kasalliklar, 'kasal':kasal, 
        'k1':k1, 'k2':k2, 'k3':k3, 'k4':k4, 'k5':k5, 'k6':k6, 'k7':k7, 'k8':k8, 'k9':k9, 
        'k10':k10, 'kasalllik_alomatlar_counts':kasalllik_alomatlar_counts})


